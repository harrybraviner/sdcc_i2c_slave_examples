// Wire Master Writer
// by Nicholas Zambetti <http://www.zambetti.com>

// Demonstrates use of the Wire library
// Writes data to an I2C/TWI slave device
// Refer to the "Wire Slave Receiver" example for use with this

// Created 29 March 2006

// This example code is in the public domain.


#include <Wire.h>

#define MCLR 7

void setup()
{
  pinMode(MCLR, OUTPUT);
  digitalWrite(MCLR, LOW);  // reset the chip
  delay(100);
  digitalWrite(MCLR, HIGH);
  
  Serial.begin(9600);
  
  Wire.begin(); // join i2c bus (address optional for master)
  
}

void loop()
{
  /*
  Wire.beginTransmission(0x51); // transmit to device #4
  Wire.write(0xf0);              // sends one byte 
  Wire.endTransmission();    // stop transmitting

  delay(100);
  */
}

void serialEvent() {
  while(Serial.available()){
    char inChar = (char)Serial.read();
    if(inChar == 'Y'){
      Wire.beginTransmission(0x51); // transmit to device #4
      Wire.write((char)0xff);              // sends one byte 
      Wire.endTransmission();    // stop transmitting
    } else {
      Wire.beginTransmission(0x51); // transmit to device #4
      Wire.write((char)0x00);              // sends one byte 
      Wire.endTransmission();    // stop transmitting
    }
    Serial.write(inChar);
  }
}
