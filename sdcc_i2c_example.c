/*
	I2C_slave_example.c
	Author:	Harry Braviner

	This example should receive an 8-bit number over I2C, and copy this into LATA to turn some LEDs on or off
	It should also transmit an 8-bit number over I2C, the LSB of which will indicate whether RC7 is high or low (to be tested using a switch)

*/

#include <pic18f2420.h>
#include <i2c.h>

// Begin configuration bits
#pragma config OSC=INTIO67, FCMEN=OFF, IESO=OFF			// CONFIG1H
/*
#pragma config PWRT=ON,	BOREN=SBORDIS, BORV=3			// CONFIG2L
#pragma config WDT=OFF, WDTPS=1					// CONFIG2H
#pragma config CCP2MX=PORTC, PBADEN=OFF, LPT1OSC=OFF, MCLRE=OFF	// CONFIG3H
#pragma config STVREN=ON, LVP=ON, XINST=ON, DEBUG=OFF		// CONFIG4L
#pragma config CP0=OFF, CP1=OFF					// CONFIG5L
#pragma config CPB=OFF, CPD=OFF					// CONFIG5H
#pragma config WRT0=OFF, WRT1=OFF				// CONFIG6L
#pragma config WRTC=OFF, WRTB=OFF, WRTD=OFF			// CONFIG6H
#pragma config EBTR0=OFF, EBTR1=OFF				// CONFIG7L
#pragma config EBTRB=OFF					// CONFIG7H
*/
// End configuration bits

char received_byte = 0x00;

void main(void){
	char PIC_I2C_address = 0xA2;	// I2C address for the PIC
	//char PIC_I2C_address = 0x51;	// I2C address for the PIC

	i2c_close();		// just in case

	i2c_open(I2C_SLAVE7B_INT, I2C_SLEW_OFF, PIC_I2C_address);

	TRISA = 0x00;	// set port A as output
	LATA = 0x00;
	TRISB = 0x00;
	LATB = 0xff;

	while(1) {
		//while(i2c_drdy()==0);	// wait until the master transmits data to us
		//LATA ^= 0xf0;
		//LATA |= 0x0e;
		//LATA &= 0xfe;
		//i2c_readchar();	// discard the address sent by master
		//while(i2c_drdy()==0);	// wait until the master transmits data to us
		received_byte = i2c_readchar();	// save the received byte
		//LATA |= 0x0d;
		//LATA &= 0xfd;
		i2c_ack();	// acknowledge received byte
		//LATA |= 0x0f;
		//LATA &= 0xff;

		LATA = received_byte;	// display the received byte using LEDs
		//LATA ^= 0xf0;
	}

}
