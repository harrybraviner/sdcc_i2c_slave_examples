#include <pic18f2420.h>
#include <i2c.h>
#include <signal.h>

#pragma config OSC=INTIO67, FCMEN=OFF, IESO=OFF

char received_byte = 0x00;

// Setup the function MSSP_interrupt_handler to be triggered on the appropriate interrupt
DEF_INTHIGH(high_handler)
	//DEF_HANDLER2(SIG_MSSP, SIG_MSSPIE, MSSP_interrupt_handler)
	DEF_HANDLER(SIG_MSSP, MSSP_interrupt_handler)
END_DEF

void main(void){
	char PIC_I2C_address = 0xa2;

	i2c_close();

	i2c_open(I2C_SLAVE7B, I2C_SLEW_OFF, PIC_I2C_address);

	PIE1bits.SSPIE = 1;		// Enable MSSP interrupt
	IPR1bits.SSPIP = 1;		// Set it to be high-priority
	RCONbits.IPEN = 1;		// Enable priority levels on interrupts
	INTCONbits.GIEH = 1;		// Enable all high priority interrupts

	TRISA = 0xff;
	LATA = 0x00;
	TRISB = 0x00;
	LATB = 0xff;

	while(1) {
		LATA = received_byte;
	}
}

SIGHANDLERNAKED(MSSP_interrupt_handler){
	LATB ^= 0xff;
	received_byte = i2c_readchar();
	i2c_ack();
}
