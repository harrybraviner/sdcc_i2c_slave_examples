all : sdcc_i2c_example.hex sdcc_i2c_slave_interrupt.hex

sdcc_i2c_example.hex : sdcc_i2c_example.c
	sdcc --use-non-free -mpic16 -p18f2420 sdcc_i2c_example.c

sdcc_i2c_slave_interrupt.hex : sdcc_i2c_slave_interrupt.c
	sdcc --use-non-free -mpic16 -p18f2420 sdcc_i2c_slave_interrupt.c
